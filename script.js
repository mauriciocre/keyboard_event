let boxTop = 200;
let boxLeft = 200;
document.addEventListener('keydown', (event) => {
    const keyName = event.key;
    
    if (event.which == 38) {
        boxTop = boxTop - 10;
    }
    
    if (event.which == 40) {
        boxTop = boxTop + 10;
    }
    
    if (event.which == 37) {
        boxLeft = boxLeft - 10;
    }
    
    if (event.which == 39) {
        boxLeft = boxLeft + 10;
    }
    document.getElementById("box").style.top = boxTop + "px";
    document.getElementById("box").style.left = boxLeft + "px";
    console.log('keydown event\n\n' + 'key: ' + keyName);
  });